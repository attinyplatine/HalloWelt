# HalloWelt

Das Hallo Welt Beispiel Programm für den Microcontroller ist ein so einfach wie möglich gehaltenes Programm. Es dient dazu, die Entwicklungsumgebung und die Verkabelung zu testen. Der Code ist so kurz und strukturiert wie möglich gehalten. Der komplette Quellcode befindet sich in der main.c.
41
Erweiterung eines Linux Embedded Systems durch einen externen Mikrocontroller | Fabian Spottog
Zu Beginn der Quellcodedatei ist eine Verzögerungszeit von 100ms definiert.
In der Main Methode wird die Setup Routine aufgerufen. Anschließend wird in der Mainloop die Funktion Loop aufgerufen und dann die zuvor definierten 100ms gewartet (sleep). Zum Warten wird die Funktion _delay_ms aus der util/delay.h verwendet.
In der Setup Methode wird der Pin PB2 als Ausgang definiert.
In der Mainloop wird der LED Pin PB2 getogglet.
Die LED beginnt zu blinken, wenn das Programm auf dem Microcontroller gestartet wird.