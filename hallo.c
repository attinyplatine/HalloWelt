/*
 * hallo.c
 *
 *  Created on: 23.03.2018
 *      Author: Fabian Spottog
 *       Title: HalloWelt
 *Beschreibung: Einfaches Testprogramm um die Umgebung zu testen.
 *
 * Das Hallo Welt Beispiel Programm für den Microcontroller ist ein so einfach wie möglich gehaltenes Programm. Es dient dazu, die Entwicklungsumgebung und die Verkabelung zu testen. Der Code ist so kurz und strukturiert wie möglich gehalten. Der komplette Quellcode befindet sich in der main.c.
41
Erweiterung eines Linux Embedded Systems durch einen externen Mikrocontroller | Fabian Spottog
Zu Beginn der Quellcodedatei ist eine Verzögerungszeit von 100ms definiert.
In der Main Methode wird die Setup Routine aufgerufen. Anschließend wird in der Mainloop die Funktion Loop aufgerufen und dann die zuvor definierten 100ms gewartet (sleep). Zum Warten wird die Funktion _delay_ms aus der util/delay.h verwendet.
In der Setup Methode wird der Pin PB2 als Ausgang definiert.
In der Mainloop wird der LED Pin PB2 getogglet.
Die LED beginnt zu blinken, wenn das Programm auf dem Microcontroller gestartet wird.
 */
#include <avr/io.h>
#include <util/delay.h>

// Konstanten
const int  msDelay = 100;

// Declarationen
void setup(void);
void loop(void);

//Hier startet das Programm
int main(void){
    // Init
	setup();
		//Main Loop
        while(1){
        	//call Loop
        	loop();
        	//Sleep
            _delay_ms(msDelay);
        }
        return 0;
}


// Init
void setup(void){
    //Setze die Ports B0 und B4 als Ausgang (LED)
    DDRB = 1<<DDB2;
	//Alle Ausänge auf Low setzen
    PORTB = 0x00;
}
//wird alle x ms aufgerufen.
void loop(void){
        PORTB ^= 1<<DDB2;   //Ausgang toggeln
}

