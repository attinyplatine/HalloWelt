/*
 * hallo.c
 *
 *  Created on: 23.03.2018
 *      Author: Fabian Spottog
 *       Title: HalloWelt
 *Beschreibung: Einfaches Test Programm um diue Umgebung zu testen.
 */
#include <avr/io.h>
#include <util/delay.h>

// Konstanten
const int  msDelay = 100;

// Declarations
void setup(void);
void loop(void);

int main(void){
    // Init
	setup();
		//Main Loop
        while(1){
        	//call Loop
        	loop();
        	//Sleep
            _delay_ms(msDelay);
        }
        return 0;
}


// Init
void setup(void){
    //Setze die Ports B0 und B4 als Ausgang (led)
    DDRB = 1<<DDB2;
	//Alle Ausänge auf Low Setzen
    PORTB = 0x00;
}
//wird alle x ms Aufgerufen.
void loop(void){
        PORTB ^= 1<<DDB2;
}

